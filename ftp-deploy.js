const path = require('path');
const FtpDeploy = require('ftp-deploy');
const ftpDeploy = new FtpDeploy();

require('dotenv').config({path: __dirname + '/.env'})

const config = {
  user: process.env.FTP_USER,
  password: process.env.FTP_PASSWORD,
  host: process.env.FTP_HOST,
  localRoot: __dirname + "/dist/",
  remoteRoot: process.env.FTP_REMOTEROOT,
  include: [
    '.htpasswd',
    '.htaccess',
    'sitemap.xml',
    '*.php',
    '*.html',
    '*.json',
    '*.ico',
    'assets/**',
    'app/**',
  ],
  exclude: [
	  '*.map',
	  'node_modules/**',
	  'node_modules/**/.*',
	  '.git/**',
	  '*.config'
  ],
  deleteRemote: false,
  forcePasv: false
};

// use with callback
ftpDeploy.deploy(config, function(err, res) {
    if (err) console.log(err);
    else console.log("finished:", res);
});
