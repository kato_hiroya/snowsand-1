// イベント
//=============================================================

import { FeatureTab } from './_feature-tab.js';

//=============================================================
// ANIMATION FRAME

window.render = () => {
  window.requestAnimationFrame(render);

  if (isTransition) return;

  // scroll
  if (pageScroll) {
    if (PC || __WW__ > tabletStyle) {
      pageScroll.updateDesktop();
    } else {
      pageScroll.updateMobile();
    }
  }
  if (stickyScroll) {
    stickyScroll.updateDesktop();
  }

  // slider
  if (dragSlider) {
    dragSlider.onUpdate();
  }

};

//=============================================================

window.resisterEvents = () => {

  addGalleryEvents();
  addHomeEvents();
  addAnchorLink();

};

//=============================================================
// LOAD完了 <初回のみ>

window.addEventListener('load', (e) => {

  loading.complete();

  // 高さ調整
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);

}, false);

//=============================================================
// SCROLL EVENT

window.scrollEvent = () => {

  window.addEventListener('scroll', (e) => {
    if (__WW__ > tabletStyle) {
      __WT__ = pageYOffset;
    }
  }, false);

  $window.addEventListener('scroll', (e) => {
    if (__WW__ <= tabletStyle) {
      __WT__ = $window.scrollTop;
    }
  }, false);

};

//=============================================================
// RESIZE EVENT

window.addEventListener('resize', (e) => {
  resizeControl.onResize();

}, false);

window.addEventListener('orientationchange', function(e) {
  resizeControl.orientationChange();

}, false);

window.resizeControl = {

  onResize() {

    const delay = 500;
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {

      __DETECT__.preDevice = __DETECT__.device;
      __DETECT__.detectDevide();
      if (__DETECT__.preDevice !== __DETECT__.device) {
        location.reload();
      }

      // 画面幅で画像切替
      let preWW = __WW__;
      this.resizeDOM();
      if ((preWW <= window.tabletStyle && __WW__ > window.tabletStyle)
        || (preWW > window.tabletStyle && __WW__ <= window.tabletStyle)) {
        loadImage();
      }

    }, delay);

  },

  orientationChange() {
    // デバイスの向きが変わったらリロード
    location.reload();

  },

  resizeDOM() {

    __WW__ = window.innerWidth;
    __WH__ = window.innerHeight;

    pageScroll.onResize();
    stickyScroll.onResize();
    dragSlider.onResize();

    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);

  },

};

//=============================================================
/* Page navi toggle */

const $pagenNaviToggle = document.querySelector('.pagenavi-toggle');
if ($pagenNaviToggle) {
  $pagenNaviToggle.addEventListener('click', (e) => {

    if ($html.classList.contains('is-navi-open')) {
      $html.classList.remove('is-navi-open');
    } else {
      $html.classList.add('is-navi-open');
    }

  }, false);
};

const $pagenNaviCloseBtn = document.querySelectorAll('.pagenavi-ovarlay, .pagenavi-inner a');
if ($pagenNaviCloseBtn) {
  $pagenNaviCloseBtn.forEach((el, i) => {

    el.addEventListener('click', (e) => {
      $html.classList.remove('is-navi-open');
    }, false);

  });
};


//=============================================================
// イベント登録 : ScrollTo

window.addAnchorLink = () => {

  const $link = document.querySelectorAll('.scroll-to');
  $link.forEach((el, i) => {

    el.addEventListener('click', (e) => {

      e.preventDefault();

      const $target = document.querySelector('[data-sct=' + el.dataset.scrollTo + ']');
      const rect = $target.getBoundingClientRect();
      const offsetY = rect.y + pageScroll.y;

      if (PC || __WW__ > tabletStyle) {

        gsap.to(window, {
          duration: 1.4,
          ease: 'power1.out',
          scrollTo: { y: offsetY, autoKill: true }
        });

      } else {

        gsap.to($window, {
          duration: 1.2,
          ease: 'power1.out',
          scrollTo: { y: $target, autoKill: true }
        });

      }

    }, false);
  });

};

//=============================================================
// イベント登録 : gallery プロフィール開閉

window.addGalleryEvents = () => {

  const $profileToggle = document.querySelector('.profile-toggle');
  if (!$profileToggle) return;

  // 開閉
  $profileToggle.addEventListener('click', (e) => {
    if (PC || __WW__ > tabletStyle) {
      e.preventDefault();
      if ($html.classList.contains('is-profile-open')) {
        $html.classList.remove('is-profile-open');
      } else {
        $html.classList.add('is-profile-open');
      }
    }

  }, false);

  // 閉じるボタン
  const $profileCloseBtn = document.querySelectorAll('.gallery-profile-ovarlay');
  if (!$profileCloseBtn) return;
  $profileCloseBtn.forEach((el, i) => {
    el.addEventListener('click', (e) => {
      $html.classList.remove('is-profile-open');
    }, false);

  });

};

//=============================================================
// イベント登録 : home各種

window.addHomeEvents = () => {
  window.featureTab = new FeatureTab();
  popupEvents();

  /* アコーディオン開閉後 ページの高さ更新 */
  const getNewHeight = () => {
    document.querySelectorAll('.accordion .toggle-icon .el1, .mb-accordion .toggle-icon .el1').forEach((el, i) => {
      el.addEventListener('transitionend', () => {
        pageScroll.getPageHeight();
      }, false);
    });
  };
  getNewHeight();

  /* ABOUT hover */
  const aboutTipsHover = () => {
    const $items = document.querySelectorAll('[data-tips]');
    if (!$items) return;
    $items.forEach((el, i) => {
      const tgt = el.dataset.tips;
      el.addEventListener('mouseenter', () => {
        document.querySelector('.sct-about').classList.add('tips-' + tgt);
      }, false);
      el.addEventListener('mouseleave', () => {
        document.querySelector('.sct-about').classList.remove('tips-' + tgt);
      }, false);
    });
  };
  aboutTipsHover();

};

//-------------------------------------
// POPUP BANNER

window.popupEvents = () => {
  let $popupCloseBtn = document.querySelector('.popup-banner-close');
  if (!$popupCloseBtn) return;

  $popupCloseBtn.addEventListener('click', evPopupClose, false);
};

window.evPopupStandby = () => {
  let $popup = document.querySelector('.popup-banner');
  if (!$popup) return;

  gsap.set('.popup-banner', {
    y: '20%',
    opacity: 0,
  });

};

window.evPopupOpen = () => {
  let $popup = document.querySelector('.popup-banner');
  if (!$popup) return;

  gsap.to('.popup-banner', {
    duration: .6,
    delay: .2,
    y: 0,
    opacity: 1,
    ease: 'Power2.inOut',
    onComplete: () => {
      document.querySelector('.popup-banner').classList.add('show');
    }
  });

};

window.evPopupClose = () => {
  let $popup = document.querySelector('.popup-banner');
  if (!$popup) return;

  gsap.to('.popup-banner-close', {
    duration: .8,
    rotation: 120,
    scale: .5,
    ease: 'Circ.out'
  });

  gsap.to('.popup-banner-thumb, .popup-banner-text', {
    duration: .4,
    delay: .3,
    y: 100,
    opacity: 0,
    ease: 'Circ.inOut',
  });

  gsap.to('.popup-banner', {
    duration: .4,
    delay: .3,
    y: '100%',
    ease: 'Circ.inOut',
    onComplete: () => {
      document.querySelector('.popup-banner').style.display = 'none';
      if (!isTransition) {
        $html.classList.add('is-banner-hidden');
      }
    }
  });

};

//=============================================================
