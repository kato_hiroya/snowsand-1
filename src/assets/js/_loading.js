// ローディング
//=============================================================

window.currentPage = null;

//=============================================================

export class Loading {

  constructor() {

    this.isLoaded = false;
    currentPage = document.querySelector('[data-barba-namespace]').dataset.barbaNamespace;

    // timeout
    this.timer = setTimeout(() => {
      this.complete();
    }, 5000);

  }

  complete() {

    if (this.isLoaded) return;

    clearTimeout(this.timer);
    this.isLoaded = true;
    $html.classList.add('is-loaded');
    init();
    render();
    this.resetPosition();
    this.removeSplash();

  }

  resetPosition() {

    window.scrollTo(0, 0);
    pageScroll.y = 0;
    __WT__ = 0;
    gsap.set('.scroll-window', {
      y: __WH__
    });

  }

  removeSplash() {

    gsap.to('.splash-logo, .splash-copy', {
      duration: 1,
      scale: 1,
      opacity: 1,
      ease: 'Power3.easeInOut'
    });
    gsap.to('.splash-indicator', {
      duration: 2,
      strokeDashoffset: 0,
      rotation: 180,
      ease: 'Power3.easeInOut',
      onComplete: () => {
        this.drawPage();
      }
    });

  }

  drawPage() {

    let delay_out = 0;

    gsap.to('.splash-indicator', {
      duration: .8,
      strokeDashoffset: 880,
      rotation: 300,
      ease: 'Power3.easeInOut'
    });
    gsap.to('.splash-inner', {
      duration: 1.2,
      delay: delay_out - .2,
      opacity: 0,
      ease: 'Power3.easeInOut'
    });
    gsap.to('.splash', {
      duration: 1.1,
      delay: delay_out,
      y: -__WH__,
      ease: 'Power3.easeInOut'
    });
    gsap.to('.scroll-window', {
      duration: 1.3,
      delay: delay_out,
      y: 0,
      ease: 'Power3.easeInOut',
      onComplete() {
        gsap.set('.splash', { display: 'none' });
        gsap.set('.scroll-window', { clearProps: 'all' });

        if (currentPage === 'home') {
          evPopupOpen();
        }
      }
    });

  }


}

//=============================================================
