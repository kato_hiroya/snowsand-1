//=============================================================

window.$html = document.querySelector('html');
window.$body = document.querySelector('body');
window.$header = document.querySelector('.header');
window.$bg = document.querySelector('.background');
window.$window = document.querySelector('.scroll-window');

window.__WH__ = window.innerHeight;
window.__WW__ = window.innerWidth;
window.__SW__ = window.screen.width;
window.__WT__ = window.scrollY || window.pageYOffset;
window.tabletStyle = 1024;
window.mobileStyle = 640;

//=============================================================

window.getImageSrc = function(d1x, d2x, mb) {
  if (!d1x && !d2x && !mb) return false;
  let src = d1x;
  if (mb && window.__DETECT__.device.any && __WW__ <= window.tabletStyle) {
    src = mb;
  } else if (d2x && window.__DETECT__.retina) {
    src = d2x;
  }
  if (window.__DETECT__.webp) {
    src = src + '.webp';
  }
  return src;
};

//
window.getExtension = function(filename) {
  return filename.slice((filename.lastIndexOf('.') - 1 >>> 0) + 2);
};

//
window.removeClass = function($target, arr) {
  //===
  const removeList = [];
  for (var n = 0; n < arr.length; n++) {
    for (let i = 0, l = $target.classList.length; i < l; ++i) {
      const reg = new RegExp('\\b' + arr[n] + '\\S+', 'g');
      const found = $target.classList[i].match(reg);
      if (found) {
        removeList.push(found[0]);
      }
      if (arr[n] === $target.classList[i]) {
        removeList.push(arr[n]);
      }
    }
  }
  for (var i = 0; i < removeList.length; i++) {
    $target.classList.remove(removeList[i]);
  }
};

//
window.getClosestElement = function(elem, selector) {
  const matchElems = document.querySelectorAll(selector);

  let prevElem = elem.previousElementSibling;
  let i = 0;
  let len = matchElems.length;

  if (typeof selector === 'undefined' || selector === '') return prevElem;

  while (prevElem) {
    for (i = 0; i < len; i++) {
      if (matchElems[i] === prevElem) return prevElem;
    }
    prevElem = prevElem.previousElementSibling;
  }

  return null;
};

//
window.getNextElement = function(node, selector) {
  if (selector && document.querySelector(selector) !== node.nextElementSibling) {
    return null;
  }
  return node.nextElementSibling;
};

//
window.getPrevElement = function(node, selector) {
  if (selector && document.querySelector(selector) !== node.previousElementSibling) {
    return null;
  }
  return node.previousElementSibling;
};

//
window.fixedEncodeURIComponent = function(str) {
  return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
    return '%' + c.charCodeAt(0).toString(16);
  });
};

//=============================================================

/*
██████╗ ███████╗████████╗███████╗ ██████╗████████╗ ██████╗ ██████╗
██╔══██╗██╔════╝╚══██╔══╝██╔════╝██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗
██║  ██║█████╗     ██║   █████╗  ██║        ██║   ██║   ██║██████╔╝
██║  ██║██╔══╝     ██║   ██╔══╝  ██║        ██║   ██║   ██║██╔══██╗
██████╔╝███████╗   ██║   ███████╗╚██████╗   ██║   ╚██████╔╝██║  ██║
╚═════╝ ╚══════╝   ╚═╝   ╚══════╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝
*/

window.__DETECT__ = {

  spec: null,

  webp: false,
  touch: false,
  mouse: false,
  retina: false,
  webgl: true,
  browser: null,
  legacy: false,
  lowspec: false,

  os: {
    win: false,
    mac: false,
    android: false,
    ios: false,
    unknown: false,
  },

  device: {
    phone: false,
    tablet: false,
    ipadpro: false,
    desktop: false,
    laptop: false,
    any: false, // tablet or mobile
  },

  preDevice: null,

  ua: window.navigator.userAgent.toLowerCase(),
  ua_ver: window.navigator.appVersion.toLowerCase(),

  once() {

    if (window.ontouchstart === null) {
      this.touch = true;
    }

    if (window.devicePixelRatio > 1) {
      this.retina = true;
    }
    if (window.navigator.platform.indexOf('Win') != -1) {
      this.os.win = true;
    }

    // this.detectSpec();
    this.detectBrows();
    this.detectDevide();
    this.detectOs();
    this.detectWebGL();
    this.detectWebp();

    this.preDevice = __DETECT__.device;

    window.$html.setAttribute('data-retina', this.retina);
    window.$html.setAttribute('data-webp', this.webp);
    window.$html.setAttribute('data-browser', this.browser);
    window.$html.setAttribute('data-touch', this.touch);

  },

  getUA(str) {
    return window.navigator.userAgent.toLowerCase().indexOf(str) != -1;
  },

  detectDevide() {

    /*
    isMobile.min.js
    -
    importの場合 -> isMobile(window.navigator.userAgent).phone
    cdnの場合 -> isMobile().phone
    */

    if (this.lowspec) {
      this.device.any = false;
      this.device.phone = false;
      this.device.tablet = false;
      this.device.desktop = true;
      window.$html.classList.add('is-desktop');
      if (window.__WW__ <= 1366) {
        this.laptop = true;
      }
    } else if (window.isMobile(window.navigator.userAgent).phone || window.__WW__ <= mobileStyle) {
      this.device.any = true;
      this.device.phone = true;
      this.device.tablet = false;
      this.device.desktop = false;
      window.$html.classList.add('is-mobile', 'is-any');
    } else if (window.isMobile(window.navigator.userAgent).tablet || window.__WW__ <= tabletStyle || (!this.getUA('ipad') && (this.getUA('macintosh') && 'ontouchend' in document))) {
      this.device.any = true;
      this.device.phone = false;
      this.device.tablet = true;
      this.device.desktop = false;
      window.$html.classList.add('is-tablet', 'is-any');
      if (window.__WW__ > 1024) {
        this.ipadpro = true;
        $html.classList.add('is-large-tablet');
      }
    } else {
      this.device.any = false;
      this.device.phone = false;
      this.device.tablet = false;
      this.device.desktop = true;
      window.$html.classList.add('is-desktop');
      if (window.__WW__ <= 1366) {
        this.laptop = true;
      }
    }

  },

  detectOs() {

    if (this.ua.indexOf('windows nt') !== -1) {
      this.os.win = true;
    } else if (this.ua.indexOf('android') !== -1) {
      this.os.android = true;
    } else if (this.ua.indexOf('iphone') !== -1 || this.ua.indexOf('ipad') !== -1) {
      this.os.ios = true;
    } else if (this.ua.indexOf('mac os x') !== -1) {
      this.os.mac = true;
    } else {
      this.os.unknown = true;
    }

  },

  detectWebGL() {
    this.canvas = document.createElement('canvas');
    var gl = this.canvas.getContext('webgl') || this.canvas.getContext('experimental-webgl');
    if (gl && gl instanceof WebGLRenderingContext) {
      this.webgl = true;
    } else {
      this.webgl = false;
    }
  },

  detectSpec() {

    if (this.retina) {

      // Laptop
      this.spec = 'Laptop, MBP Corei3 ~ Corei5 class';

      // Laptop HiEnd Corei7
      if (window.__SW__ <= 1680 && window.navigator.hardwareConcurrency >= 8) {
        this.spec = 'Laptop HiEnd, MBP over Corei7';
      }

      // Desktop Corei5
      if (window.__SW__ > 1680 && window.navigator.hardwareConcurrency >= 6) {
        this.spec = 'Desktop MidEnd, iMac over Corei5 相当';
      }

      // Desktop Corei7
      if (window.__SW__ > 1680 && window.navigator.hardwareConcurrency >= 8) {
        this.spec = 'Desktop HiEnd, iMac over Corei7';
      }

      // Large Desktop
      if (window.__WW__ >= 1980) {
        this.spec = 'Large Desktop';
      }

    } else {

      // Lowend
      this.spec = 'Win or Leagcy Mac';
      // Win Midend
      if (window.navigator.hardwareConcurrency >= 6 || window.__WW__ <= 1366) {
        this.spec = 'Desktop MidEnd';
      }

    }

  },

  detectBrows() {

    if (this.ua.indexOf('edg') !== -1) {
      this.browser = 'edge';
      this.legacy = true;
    } else if (this.ua.indexOf('iemobile') !== -1) {
      this.browser = 'iemobile';
      this.legacy = true;
      this.lowspec = true;
    } else if (this.ua.indexOf('trident/7') !== -1) {
      // this.browser = "ie11";
      this.browser = 'ie';
      this.legacy = true;
      this.lowspec = true;
    } else if (this.ua.indexOf('msie') !== -1 && this.ua.indexOf('opera') === -1) {
      this.legacy = true;
      this.lowspec = true;
      this.browser = 'ie';
    } else if (this.ua.indexOf('chrome') !== -1 && this.ua.indexOf('edge') === -1) {
      this.browser = 'chrome';
      $html.classList.add('chrome');
    } else if (this.ua.indexOf('safari') !== -1 && this.ua.indexOf('chrome') === -1) {
      this.browser = 'safari';
      $html.classList.add('safari');
    } else if (this.ua.indexOf('opera') !== -1) {
      this.browser = 'opera';
    } else if (this.ua.indexOf('firefox') !== -1) {
      this.browser = 'firefox';
    } else {
      this.browser = 'unknown';
    }

    //
    if (this.lowspec) { window.$html.classList.add('is-lowspec'); }
    if (this.legacy) { window.$html.classList.add('is-legacy'); }

  },

  detectWebp() {
    if (this.canvas.toDataURL('image/webp').indexOf('data:image/webp') == 0) {
      this.webp = true;
    } else {
      this.webp = false;
    }
  },

};

//=============================================================

window.__EASE__ = {

  timelines: {
    'power1': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'power2': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'power3': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'power4': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Linear': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Cubic': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Quad': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Quint': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Strong': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Elastic': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Back': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Bounce': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Sine': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Expo': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },
    'Circ': {
      'in': { tl: null, p: 0, },
      'out': { tl: null, p: 0, },
      'inOut': { tl: null, p: 0, },
    },

  },

  once() {

    var _this = this;
    Object.keys(_this.timelines).forEach(function(key) {
      Object.keys(_this.timelines[key]).forEach(function(k) {
        var ease = key + '.' + k;
        _this.timelines[key][k].tl = gsap.timeline();
        _this.timelines[key][k].tl.to(_this.timelines[key][k], {
          p: 1,
          duration: 2,
          ease: ease,
        });
      });
    });

  },

  getEase(power, ease, progress) {

    //===========
    // progress -> 0 ~ 1
    //===========
    this.timelines[power][ease].tl.progress(progress).pause();
    return this.timelines[power][ease].p;

  }

};

window.EASING = (power = power1, ease = inOut, progress) => {
  let progress_by_ease = __EASE__.getEase(power, ease, Number(progress));
  return progress_by_ease;
};


//=============================================================

if (window.__WW__ === 0) {
  location.reload();
} else {
  window.__DETECT__.once();
  window.__EASE__.once();
};

// console.log(window.__DETECT__);
