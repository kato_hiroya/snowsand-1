export class ViewIn {

  constructor() {
    this.standby();
    this.register();
  }

  register() {

    this.$inview = document.querySelectorAll('.js-inview');
    this.targets = [...this.$inview];
    if (this.targets.length) {
      let options = {
        threshold: 0
      };
      this.observer = new IntersectionObserver(this.listener, options);
      this.targets.forEach((e, i) => {
        this.observer.observe(e);
      });
    }

  }

  listener(changes) {

    const _this = this;

    changes.forEach((e, i) => {
      const el = e.target;
      const isIntersecting = e.isIntersecting;
      if (isIntersecting) {
        el.dataset.visible = 1;
        if (!el.classList.contains('is-inview')) {

          el.classList.add('is-inview');

        }
      } else {
        el.dataset.visible = 0;
        if (el.classList.contains('is-inview')) {

          el.classList.remove('is-inview');

        }
      }
    });

  }

  standby() {

    const $riseTxt = document.querySelectorAll('.rise-txt, .rise-txt-p, .f-rise-txt, .f-rise-txt-p');
    $riseTxt.forEach((el, i) => {
      const html = el.innerHTML;
      el.innerHTML = '<span class="rt-in">' + html + '</span>';
    });

  }

}

//=============================================================
