import Vue from 'vue';

if (process.browser) {

  require('../assets/js/vendor.js');
  require('../assets/js/0_env.js');
  require('../assets/js/1_init.js');
  require('../assets/js/2_page-transition.js');
  require('../assets/js/3_events.js');

}